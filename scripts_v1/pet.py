from using_requests import url_base


url_upload = "/uploadImage"
url_base_pet = f"{url_base}/pet"
url_base_pet_status = f"{url_base_pet}/findByStatus?status="


def check_ides_are_equals(id, json):
    if id == json["id"]:
        return True
    return False


def check_statuses_are_equals(status, json):
    for each in json:
        if status != each["status"]:
            return False
    return True


def create_dict_id(id):
    return {"id": id}


def create_dict_category(id, name):
    category = {
                    "category": {
                        "id": id,
                        "name": name
                        }
                }
    return category


def create_dict_name(name):
    return {"name": name}


def create_dict_photo(url_photo):
    return {"photoUrls": [url_photo]}


def create_dict_tags(id, name):
    tags = {"tags": [
        {
                "id": id,
                "name": name
        }
    ]
    }
    return tags


def create_dict_status(status):
    return {"status": status}


def create_dict_metadata(metadata):
    return {"additionalMetadata": metadata}


def merge_dicts(*dict_args):
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    print(result)
    return result


def are_dict_equals(first_dict, second_dict):
    print("\n", first_dict)
    print("\n", second_dict)
    if first_dict == second_dict:
        return True
    else:
        return False


def is_dict1_include_items_dict2(dict1, dict2):
    """
    using if dict2 has less keys than dict1
    """
    keys_dict2 = dict2.keys()
    keys_where_values_are_different = []
    for key in keys_dict2:
        if dict1[key] != dict2[key]:
            keys_where_values_are_different.append(key)
    if not keys_where_values_are_different:
        return True
    else:
        return False


def is_json_contains_jpg(dict_json):
    if dict_json["message"].find(".jpg") == -1:
        return False
    return True
