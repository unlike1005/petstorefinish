from using_requests import url_base

url_user = "/user"
url_base_user = f"{url_base}{url_user}"
url_create_with_list = "/createWithList"
url_create_with_array = "/createWithArray"
url_login = "/login"
url_logout = "/logout"
url_base_user_list = f"{url_base_user}{url_create_with_list}"
url_base_user_array = f"{url_base_user}{url_create_with_array}"
url_base_user_login = f"{url_base_user}{url_login}"
url_base_user_logout = f"{url_base_user}{url_logout}"
url_base_user_slash = f"{url_base_user}/"


def create_dict_user_name(user_name):
    return {"username": user_name}


def create_dict_first_name(first_name):
    return {"firstName": first_name}


def create_dict_last_name(last_name):
    return {"lastName": last_name}


def create_dict_email(email):
    return {"email": email}


def create_dict_password(password):
    return {"password": password}


def create_dict_phone(phone):
    return {"phone": phone}


def create_dict_user_status(user_status):
    return {"userStatus": user_status}


def create_list_of_dictionaries(*dicts):
    list_of_dicts = []
    for each in dicts:
        list_of_dicts.append(each)
    return list_of_dicts


def check_names_are_equals(name, json):
    if name == json["username"]:
        return True
    return False


def is_message_contains_some_str(dict_json, some_data):
    if isinstance(some_data, int):
        some_data = str(some_data)
    if dict_json["message"].find(some_data) == -1:
        return False
    return True


def get_user_id(some_dict):
    return some_dict["id"]
