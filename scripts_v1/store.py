from using_requests import url_base

url_store = "/store"
url_base_store = f"{url_base}{url_store}"
url_inventory = "/inventory"
url_order = "/order"
url_store_inventory = f"{url_base_store}{url_inventory}"
url_store_order = f"{url_base_store}{url_order}"
url_store_order_slash = f"{url_store_order}/"


def create_dict_order(id, pet_id, quantity,
                      ship_date, status, complete):
    order = {
        "id": id,
        "petId": pet_id,
        "quantity": quantity,
        "shipDate": ship_date,
        "status": status,
        "complete": bool(complete)
    }
    return order


def int_to_str(int_value):
    return str(int_value)
