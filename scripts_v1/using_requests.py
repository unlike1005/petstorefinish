import requests
import logging
url_base = "://petstore.swagger.io/v2"

request_header = {
    "Accept": "application/json", "Authorization": "special-key",
    "Content-Type": "application/x-www-form-urlencoded"
}


def join_url(url1, url2):
    if isinstance(url2, int):
        url2 = str(url2)
    if url1[-1] != "/" and url2[0] != "/" and url2[0] != ":":
        link = f"{url1}/{url2}"
    else:
        link = f"{url1}{url2}"
    return link


def return_json(response, text=False):
    print(response)
    print(response.status_code)
    if response.status_code != 200:
        print("Check arguments")
        return False
    else:
        if text is True:
            print(response.text)
            return response.text
        return response.json()


def request(method, url, header=None, params=None, body=None, files=None):
    logging.info("-- REQUEST ---")
    logging.info(f"REQUEST URL: {url}")
    logging.info(f"REQUEST METHOD: {method}")
    logging.info(f"REQUEST HEADERS: {header}")
    logging.info(f"REQUEST PARAMS: {params}")
    logging.info(f"REQUEST BODY: {body}")
    logging.info(f"FILES: {files}")

    if files is not None:
        file = {"file": open(files, "rb")}
        response = requests.request(method=method, url=url, headers=header,
                                    params=params, json=body, files=file)
    else:
        response = requests.request(method=method, url=url, headers=header,
                                    params=params, json=body)

    logging.info("-- RESPONSE ---")
    logging.info(f"RESPONSE STATUS CODE: {response.status_code}")
    logging.info(f"RESPONSE HEADERS: {response.headers}")
    logging.info(f"RESPONSE: {response.text}")
    return return_json(response)
