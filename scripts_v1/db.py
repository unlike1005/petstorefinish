import random


import mysql.connector as mysql


def connect_to_db():
    db = mysql.connect(
        host="localhost",
        user="root",
        passwd="password",
        database="pets_store_db",
    )
    return db, db.cursor()


def create_database(cursor):
    cursor.execute("CREATE DATABASE pet_store_db")
    cursor.execute("SHOW DATABASES")
    print(cursor.fetchall())


def create_table(cursor):
    cursor.execute("""CREATE TABLE pets (data_key VARCHAR(30),
     data_value VARCHAR(30))""")
    cursor.execute("SHOW TABLES")
    print(cursor.fetchall())


def delete_table(cursor):
    cursor.execute("DROP TABLE pets")
    cursor.execute("SHOW TABLES")
    print(cursor.fetchall())


def insert_values(cursor):
    id = random.randint(0, 9223372036854775807)
    category_id = random.randint(0, 100)
    category_name = random.choice(['dogs', 'cats', 'mice'])
    tag_id = random.randint(0, 100)
    order_id = random.randint(0, 10000)
    user_id = random.randint(5, 10000)

    query = """INSERT INTO pets
               (data_key, data_value)
               VALUES (%s, %s)"""
    values = [
        ("id", f"{id}"),
        ("category_id", f"{category_id}"),
        ("category_name", f"{category_name}"),
        ("name", f"pet_name{id}"),
        ("photoUrls", "http://photo.com/pet_name"),
        ("tags_id", f"{tag_id}"),
        ("tags_name", f"tag_name{tag_id}"),
        ("status_available", "available"),
        ("status_pending", "pending"),
        ("status_sold", "sold"),
        ("file", "/home/alla/Downloads/pets.jpg"),
        ("category_id_update", f"{category_id + 1}"),
        ("category_name_update", f"{category_name}_update"),
        ("name_update", f"pet_name{id + 1}"),
        ("photoUrls_update", "http://photo.com/pet_name/upd}"),
        ("tags_id_update", f"{tag_id + 1}"),
        ("tags_name_update", f"tag_name{tag_id + 1}"),
        ("additionalMetadata", "add image"),
        ("order_id", f"{order_id}"),
        ("quantity", "1"),
        ("ship_date", "2021-05-14T13:24:12.192+0000"),
        ("order_status", "placed"),
        ("complete", "True"),
        ("user_id", f"{user_id}"),
        ("username", "alla4567"),
        ("firstName", "alla"),
        ("lastName", "Alla"),
        ("email", "alla@alla.com"),
        ("password", "qwe"),
        ("phone", "12345678"),
        ("userStatus", f"{0}"),
        ("user_id_1", f"{user_id - 1}"),
        ("username_1", f"alla4567{user_id - 1}"),
        ("firstName_1", f"alla{user_id - 1}"),
        ("lastName_1", f"Alla{user_id - 1}"),
        ("email_1", f"alla{user_id - 1}@alla.com"),
        ("password_1", f"qwe{user_id - 1}"),
        ("phone_1", "123456781"),
        ("userStatus_1", f"{1}"),
        ("user_id_2", f"{user_id - 2}"),
        ("username_2", f"alla4567{user_id - 2}"),
        ("firstName_2", f"alla{user_id - 2}"),
        ("lastName_2", f"Alla{user_id - 2}"),
        ("email_2", f"alla{user_id - 2}@alla.com"),
        ("password_2", f"qwe{user_id - 2}"),
        ("phone_2", "123456782"),
        ("userStatus_2", f"{2}"),
        ("user_id_3", f"{user_id - 3}"),
        ("username_3", f"alla4567{user_id - 3}"),
        ("firstName_3", f"alla{user_id - 3}"),
        ("lastName_3", f"Alla{user_id - 3}"),
        ("email_3", f"alla{user_id - 3}@alla.com"),
        ("password_3", f"qwe{user_id - 3}"),
        ("phone_3", "123456783"),
        ("userStatus_3", f"{3}"),
        ("user_id_4", f"{user_id - 4}"),
        ("username_4", f"alla4567{user_id - 4}"),
        ("firstName_4", f"alla{user_id - 4}"),
        ("lastName_4", f"Alla{user_id - 4}"),
        ("email_4", f"alla{user_id - 4}@alla.com"),
        ("password_4", f"qwe{user_id - 4}"),
        ("phone_4", "1234567834"),
        ("userStatus_4", f"{4}")
    ]
    cursor.executemany(query, values)
    print(cursor.rowcount, "records inserted")


def one_execute(cursor, query):
    cursor.execute(query)
    result = cursor.fetchall()
    return result


def parse(sql_answer):
    arr = []
    for row in sql_answer:
        arr.append(*row)
    if len(arr) == 1:
        try:
            int_value = int(arr[0])
            return int_value
        except Exception:
            return arr[0]
    return arr


def get_data(cursor, value_of_key):
    query = f"SELECT data_value FROM pets WHERE data_key=" \
            f"\"{value_of_key}\";"
    sql_answer = one_execute(cursor, query)
    print(sql_answer)
    return parse(sql_answer)


def close_connection(db):
    db.commit()
    db.close()


if __name__ == "__main__":
    db, cursor = connect_to_db()
    delete_table(cursor)
    create_table(cursor)
    insert_values(cursor)
    close_connection(db)
