*** Settings ***
Resource   /home/alla/PycharmProjects/PetsStory/tests_v1/common.robot
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/db.py
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/pet.py
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/using_requests.py
Variables  /home/alla/PycharmProjects/PetsStory/scripts_v1/pet.py
Library  BuiltIn
#Test Setup  Update Yaml File
# --variable type_of_protocol:http

*** Variables ***
${url_base_plus_pet}=  ${url_base_pet}
${url_for_get_by_status}=  ${url_base_pet_status}
${url_for_upload_file}=  ${url_upload}

*** Keywords ***
Create Dictionary Category ${cursor}|${category_id}|${category_name}
    ${id}                 db.get_data                       ${cursor}      ${category_id}
    ${name}               db.get_data                       ${cursor}      ${category_name}
    ${category_dict}      pet.create_dict_category          ${id}          ${name}
    [Return]              ${category_dict}

Create Dictionary Name ${cursor}|${pet_name}
    log to console  \nname from create dictionary: ${pet_name}
    ${name}               db.get_data                       ${cursor}      ${pet_name}
    log to console  \nname from create dictionary: ${name}
    ${name_dict}          pet.create_dict_name              ${name}
    [Return]              ${name_dict}

Create Dictionary PhotoUrl ${cursor}|${photo_urls}
    ${urls}               db.get_data                       ${cursor}      ${photo_urls}
    ${urls_dict}          pet.create_dict_photo             ${urls}
    [Return]              ${urls_dict}

Create Dictionary Tags ${cursor}|${tags_id}|${tags_name}
    ${id}                 db.get_data                       ${cursor}      ${tags_id}
    ${name}               db.get_data                       ${cursor}      ${tags_name}
    ${tags_dict}          pet.create_dict_tags              ${id}          ${name}
    [Return]              ${tags_dict}

Create Dictionary Status ${cursor}|${pet_status}
    ${status}             db.get_data                       ${cursor}      ${pet_status}
    ${status_dict}        pet.create_dict_status            ${status}
    [Return]              ${status_dict}

Create Dictionary Metadata ${cursor}|${additionalMetadata}
    ${metadata}           db.get_data                       ${cursor}      ${additionalMetadata}
    ${metadata_dict}      pet.create_dict_metadata          ${metadata}
    [Return]              ${file_dict}

Create Dictionary Pet ${cursor}|${id}|${category_id}|${category_name}|${name}|${photoUrls}|${tags_id}|${tags_name}|${status}
    ${id_dict}            Create Dictionary Id ${cursor}|${id}
    ${category_dict}      Create Dictionary Category ${cursor}|${category_id}|${category_name}
    ${name_dict}          Create Dictionary Name ${cursor}|${name}
    ${photo_urls_dict}    Create Dictionary PhotoUrl ${cursor}|${photoUrls}
    ${tags_dict}          Create Dictionary Tags ${cursor}|${tags_id}|${tags_name}
    ${status_dict}        Create Dictionary Status ${cursor}|${status}
    ${merge_dictionary}   pet.merge_dicts                   ${id_dict}  ${category_dict}  ${name_dict}  ${photo_urls_dict}  ${tags_dict}  ${status_dict}
    [Return]              ${merge_dictionary}


*** Test Cases ***
#TC_01 add new pet
#    [Tags]  post
#    ${db}  ${cursor}      Connect To Database
#    ${payload}   Create Dictionary Pet ${cursor}|id|category_id|category_name|name|photoUrls|tags_id|tags_name|status_available
#    db.close_connection   ${db}
#    ${responce} =  add new pet  ${payload}  ${type_of_protocol}
#    ${result}             pet.are_dict_equals               ${payload}  ${responce}
#    should be true        ${result}

TC_01 add new pet
    [Tags]  post
    ${db}  ${cursor}      Connect To Database
    ${merge_dictionary}   Create Dictionary Pet ${cursor}|id|category_id|category_name|name|photoUrls|tags_id|tags_name|status_available
    db.close_connection   ${db}
    ${url}                using_requests.join_url           ${type_of_protocol}  ${url_base_plus_pet}
    ${json}               using_requests.request            method=post  url=${url}  body=${merge_dictionary}
    ${result}             pet.are_dict_equals               ${merge_dictionary}  ${json}
    should be true        ${result}

TC_02 update name and status
    [Tags]  post
    ${db}  ${cursor}      Connect To Database
    ${id}                 db.get_data                       ${cursor}      id
    ${id_dict}            Create Dictionary ID ${cursor}|id
    ${name_dict}          Create Dictionary Name ${cursor}|name_update
    db.close_connection   ${db}
    ${url}                Url One Object ${id}|${url_base_plus_pet}
    ${before}             using_requests.request                           method=get   url=${url}
    ${json}               using_requests.request                           method=post  url=${url}  header=${headers}  params=${name_dict}
    Should Not Be False ${json}
    ${after}              using_requests.request                           method=get   url=${url}
    ${result}             pet.are_dict_equals               ${before}  ${after}
    Should Be False ${result}

TC_03 upload image
    [Tags]  post
    ${db}  ${cursor}      Connect To Database
    ${id}                 db.get_data                       ${cursor}      id
    ${file}               db.get_data                       ${cursor}      file
    db.close_connection   ${db}
    ${url_pet}            Url One Object ${id}|${url_base_plus_pet}
    ${url}                using_requests.join_url           ${url_pet}     ${url_for_upload_file}
    ${json}               using_requests.request            method=post    url=${url}  files=${file}
    ${result}             pet.is_json_contains_jpg                ${json}
    should be true        ${result}


TC_04 get pet by id
    [Tags]  get
    ${db}  ${cursor}      Connect To Database
    ${id}                 db.get_data                       ${cursor}      id
    db.close_connection   ${db}
    ${url}                Url One Object ${id}|${url_base_plus_pet}
    ${json}               using_requests.request            method=get    url=${url}
    ${result}             pet.check_ides_are_equals         ${id}          ${json}
    should be true        ${result}

TC_05 get pet by status
    [Tags]  get
    ${db}  ${cursor}      Connect To Database
    ${status}             db.get_data                       ${cursor}      status_available
    db.close_connection   ${db}
    ${url_with_protocol}  using_requests.join_url           ${type_of_protocol}  ${url_for_get_by_status}
    ${url}                using_requests.join_url           ${url_with_protocol}  ${status}
    ${json}               using_requests.request            method=get    url=${url}
    ${result}             pet.check_statuses_are_equals     ${status}      ${json}
    should be true        ${result}

TC_06 update pet
    [Tags]  put
    ${db}  ${cursor}      Connect To Database
    ${merge_dictionary}   Create Dictionary Pet ${cursor}|id|category_id_update|category_name_update|name_update|photoUrls|tags_id_update|tags_name_update|status_pending
    ${id}                 db.get_data                       ${cursor}      id
    db.close_connection   ${db}
    ${url}                Url One Object ${id}|${url_base_plus_pet}
    ${before}             using_requests.request            method=get    url=${url}
    ${url}                using_requests.join_url           ${type_of_protocol}  ${url_base_plus_pet}
    ${after}              using_requests.request            method=put    url=${url}   body=${merge_dictionary}
    ${result}             pet.are_dict_equals               ${merge_dictionary}  ${after}
    should be true        ${result}
    ${result1}            pet.are_dict_equals               ${before}  ${after}
    Should Be False ${result1}

TC_07 delete pet
    [Tags]  delete
    ${db}  ${cursor}      Connect To Database
    ${id}                 db.get_data                       ${cursor}      id
    db.close_connection   ${db}
    ${url}                Url One Object ${id}|${url_base_plus_pet}
    ${result}             using_requests.request            method=delete    url=${url}
    Should Not Be False ${result}
    ${result1}            using_requests.request            method=get    url=${url}
    Should Be False ${result1}
