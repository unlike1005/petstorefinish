*** Settings ***
Variables  /home/alla/PycharmProjects/PetsStory/scripts_v1/using_requests.py

*** Variables ***
${type_of_protocol}=  https
${headers}=  ${request_header}

*** Keywords ***
Connect To Database
    ${db}  ${cursor}  db.connect_to_db
    [Return]  ${db}  ${cursor}

Should Be False ${result}
    ${bool_false}         Convert To Boolean                False
    Should Be Equal       ${result}                         ${bool_false}

Should Not Be False ${result}
    ${bool_false}         Convert To Boolean                False
    Should Not Be Equal       ${result}                         ${bool_false}

Create Dictionary Id ${cursor}|${some_id}
    ${id}                 db.get_data                       ${cursor}      ${some_id}
    ${id_dict}            pet.create_dict_id                ${id}
    [Return]              ${id_dict}

Url One Object ${id}|${url_intermediate}
    ${url_with_protocol}  using_requests.join_url           ${type_of_protocol}   ${url_intermediate}
    ${url}                using_requests.join_url           ${url_with_protocol}  ${id}
    [Return]              ${url}
