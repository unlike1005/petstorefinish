*** Settings ***
Resource   /home/alla/PycharmProjects/PetsStory/tests_v1/common.robot
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/db.py
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/user.py
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/pet.py
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/using_requests.py
Variables  /home/alla/PycharmProjects/PetsStory/scripts_v1/user.py
Library  BuiltIn
#Test Setup  Update Yaml File
# --variable type_of_protocol:http

*** Variables ***
${url_for_post_list}=  ${url_base_user_list}
${url_for_post_array}=  ${url_base_user_array}
${url_for_login}=  ${url_base_user_login}
${url_for_logout}=  ${url_base_user_logout}
${url_user}=  ${url_base_user}
${url_user_slash}=  ${url_base_user_slash}

*** Keywords ***
Create Dictionary User Name ${cursor}|${nick}
    ${user_name}          db.get_data                       ${cursor}      ${nick}
    ${user_name_dict}     user.create_dict_user_name        ${user_name}
    [Return]              ${user_name_dict}

Create Dictionary First Name ${cursor}|${user_first_name}
    ${first_name}         db.get_data                       ${cursor}      ${user_first_name}
    ${first_name_dict}    user.create_dict_first_name       ${first_name}
    [Return]              ${first_name_dict}

Create Dictionary Last Name ${cursor}|${user_last_name}
    ${last_name}         db.get_data                        ${cursor}      ${user_last_name}
    ${last_name_dict}    user.create_dict_last_name         ${last_name}
    [Return]             ${last_name_dict}

Create Dictionary Email ${cursor}|${user_email}
    ${email}             db.get_data                        ${cursor}      ${user_email}
    ${email_dict}        user.create_dict_email             ${email}
    [Return]             ${email_dict}

Create Dictionary Password ${cursor}|${user_password}
    ${password}          db.get_data                        ${cursor}      ${user_password}
    ${password_dict}     user.create_dict_password          ${password}
    [Return]             ${password_dict}

Create Dictionary Phone ${cursor}|${phone_number}
    ${number}            db.get_data                        ${cursor}      ${phone_number}
    ${number_dict}       user.create_dict_phone             ${number}
    [Return]             ${number_dict}

Create Dictionary User Status ${cursor}|${user_status}
    ${status}            db.get_data                        ${cursor}      ${user_status}
    ${status_dict}       user.create_dict_user_status       ${status}
    [Return]             ${status_dict}

Create Dictionary User ${cursor}|${id}|${nick}|${user_first_name}|${user_last_name}|${user_email}|${user_password}|${phone_number}|${user_status}
    ${id_dict}            Create Dictionary Id ${cursor}|${id}
    ${nick_dict}          Create Dictionary User Name ${cursor}|${nick}
    ${first_name_dict}    Create Dictionary First Name ${cursor}|${user_first_name}
    ${last_name_dict}     Create Dictionary Last Name ${cursor}|${user_last_name}
    ${email_dict}         Create Dictionary Email ${cursor}|${user_email}
    ${password_dict}      Create Dictionary Password ${cursor}|${user_password}
    ${number_dict}        Create Dictionary Phone ${cursor}|${phone_number}
    ${status_dict}        Create Dictionary User Status ${cursor}|${user_status}
    ${merge_dictionary}   pet.merge_dicts                   ${id_dict}  ${nick_dict}  ${first_name_dict}  ${last_name_dict}  ${email_dict}  ${password_dict}  ${number_dict}  ${status_dict}
    [Return]              ${merge_dictionary}

Check json ${json}|${message}
    Should Not Be False ${json}
    ${result}             user.is_message_contains_some_str     ${json}  ${message}
    should be true        ${result}

Get user by username ${name}
    ${url}                Url One Object ${name}|${url_user_slash}
    ${json}               using_requests.request            method=get     url=${url}
    [Return]              ${json}

*** Test Cases ***
TC_01 create user
    [Tags]  post
    ${db}  ${cursor}      Connect To Database
    ${merge_dictionary}   Create Dictionary User ${cursor}|user_id|username|firstName|lastName|email|password|phone|userStatus
    db.close_connection   ${db}
    ${user_id}            get_user_id                       ${merge_dictionary}
    ${url}                using_requests.join_url           ${type_of_protocol}  ${url_user}
    ${json}               using_requests.request            method=post  url=${url}  body=${merge_dictionary}
    Check json ${json}|${user_id}

TC_02 create users with array
    [Tags]  post
    ${db}  ${cursor}      Connect To Database
    ${merge_dictionary}   Create Dictionary User ${cursor}|user_id|username|firstName|lastName|email|password|phone|userStatus
    ${merge_dictionary1}  Create Dictionary User ${cursor}|user_id_1|username_1|firstName_1|lastName_1|email_1|password_1|phone_1|userStatus_1
    db.close_connection   ${db}
    ${list_of_dicts}      create_list_of_dictionaries       ${merge_dictionary}  ${merge_dictionary1}
    ${url}                using_requests.join_url           ${type_of_protocol}  ${url_for_post_array}
    ${json}               using_requests.request            method=post  url=${url}  body=${list_of_dicts}
    Check json ${json}|ok

TC_03 create users with list
    [Tags]  post
    ${db}  ${cursor}      Connect To Database
    ${merge_dictionary}   Create Dictionary User ${cursor}|user_id_2|username_2|firstName_2|lastName_2|email_2|password_2|phone_2|userStatus_2
    ${merge_dictionary1}  Create Dictionary User ${cursor}|user_id_3|username_3|firstName_3|lastName_3|email_3|password_3|phone_3|userStatus_3
    db.close_connection   ${db}
    ${list_of_dicts}      create_list_of_dictionaries       ${merge_dictionary}  ${merge_dictionary1}
    ${url}                using_requests.join_url           ${type_of_protocol}  ${url_for_post_list}
    ${json}               using_requests.request            method=post    url=${url}  body=${list_of_dicts}
    Check json ${json}|ok

TC_04 get users by nick
    [Tags]  get
    ${db}  ${cursor}      Connect To Database
    ${name}               db.get_data                       ${cursor}      username
    db.close_connection   ${db}
    ${url}                Url One Object ${name}|${url_user_slash}
    ${json}               using_requests.request            method=get     url=${url}
    ${result}             user.check_names_are_equals       ${name}        ${json}
    should be true        ${result}

TC_05 user login
    [Tags]  get
    ${db}  ${cursor}      Connect To Database
    ${name}               db.get_data                       ${cursor}      username
    ${password}           db.get_data                       ${cursor}      password
    ${name_dict}          Create Dictionary User Name ${cursor}|${name}
    ${password_dict}      Create Dictionary Password ${cursor}|${password}
    db.close_connection   ${db}
    ${merge_dictionary}   pet.merge_dicts                   ${name_dict}  ${password_dict}
    ${url}                using_requests.join_url           ${type_of_protocol}  ${url_for_login}
    ${json}               using_requests.request            method=get     url=${url}   body=${merge_dictionary}
    Should Not Be False ${json}
    ${result}             user.is_message_contains_some_str  ${json}  logged in user session:
    should be true        ${result}

TC_06 user logout
    [Tags]  get
    ${url}                using_requests.join_url           ${type_of_protocol}  ${url_for_logout}
    ${json}               using_requests.request            method=get     url=${url}
    Check json ${json}|ok

TC_07 user update
    [Tags]  put
    ${db}  ${cursor}      Connect To Database
    ${name}               db.get_data                       ${cursor}      username
    ${merge_dictionary}   Create Dictionary User ${cursor}|user_id|username|firstName_2|lastName_2|email_2|password_2|phone_2|userStatus_2
    db.close_connection   ${db}
    ${id}                 get_user_id                       ${merge_dictionary}
    ${url}                Url One Object ${name}|${url_user_slash}
    ${json}               using_requests.request            method=put     url=${url}   body=${merge_dictionary}
    Check json ${json}|${id}

TC_08 user delete
    ${db}  ${cursor}      Connect To Database
    ${name}               db.get_data                       ${cursor}      username
    ${url}                Url One Object ${name}|${url_user_slash}
    ${json}               using_requests.request            method=delete  url=${url}
    Check json ${json}|${name}
