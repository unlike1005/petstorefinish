*** Settings ***
Resource   /home/alla/PycharmProjects/PetsStory/tests_v1/common.robot
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/db.py
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/store.py
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/pet.py
Library    /home/alla/PycharmProjects/PetsStory/scripts_v1/using_requests.py
Variables  /home/alla/PycharmProjects/PetsStory/scripts_v1/store.py
Library  BuiltIn

*** Variables ***
${url_for_get_inventory}=  ${url_store_inventory}
${url_for_post}=           ${url_store_order}
${url_for_one_order}=      ${url_store_order_slash}
${url_for_get_inventory}=  ${url_store_inventory}


*** Test Cases ***
TC_01 post order
    [Tags]  post
    ${db}  ${cursor}      Connect To Database
    ${order_id}           db.get_data              ${cursor}      order_id
    ${pet_id}             db.get_data              ${cursor}      id
    ${quantity}           db.get_data              ${cursor}      quantity
    ${ship_date}          db.get_data              ${cursor}      ship_date
    ${order_status}       db.get_data              ${cursor}      order_status
    ${complete}           db.get_data              ${cursor}      complete
    db.close_connection   ${db}
    ${order_dict}         store.create_dict_order  ${order_id}    ${pet_id}  ${quantity}  ${ship_date}  ${order_status}  ${complete}
    ${url}                using_requests.join_url                 ${type_of_protocol}     ${url_for_post}
    ${json}               using_requests.request                  method=post             url=${url}   body=${order_dict}
    ${result}             pet.are_dict_equals      ${order_dict}  ${json}
    should be true        ${result}

TC_02 get_order
    [Tags]  get
    ${db}  ${cursor}      Connect To Database
    ${order_id}           db.get_data              ${cursor}      order_id
    db.close_connection   ${db}
    ${url}                Url One Object ${order_id}|${url_for_one_order}
    ${order_id_str}       store.int_to_str                        ${order_id}
    ${json}               using_requests.request                  method=get              url=${url}   params=${order_id_str}
    ${result}             pet.check_ides_are_equals               ${order_id}             ${json}
    should be true        ${result}

TC_03 get_inventory
    [Tags]  get
    ${url}                using_requests.join_url                 ${type_of_protocol}     ${url_for_get_inventory}
    ${result}             using_requests.request                  method=get              url=${url}
    Should Not Be False ${result}

TC_04 delete_order
    [Tags]  delete
    ${db}  ${cursor}      Connect To Database
    ${order_id}           db.get_data              ${cursor}      order_id
    db.close_connection   ${db}
    ${url}                Url One Object ${order_id}|${url_for_one_order}
    ${order_id_str}       store.int_to_str                        ${order_id}
    ${result}             using_requests.request                  method=delete           url=${url}   params=${order_id_str}
    Should Not Be False ${result}
    ${result1}            using_requests.request                  method=get              url=${url}   params=${order_id_str}
    Should Be False ${result1}
